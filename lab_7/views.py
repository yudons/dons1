from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
from django.core import serializers
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    page = request.GET.get('page',1)
    paginator = Paginator(mahasiswa_list, 10)
    try:
        mahasiswa = paginator.page(page)
    except PageNotAnInteger:
        mahasiswa = paginator.page(1)
    except EmptyPage:
        mahasiswa = paginator.page(paginator.num_pages)
    friend_list = Friend.objects.all()
    html = 'lab_7/lab_7.html'
    response = {"mahasiswa_list": mahasiswa, "friend_list": friend_list}
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar-teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        name = request.POST['name']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return HttpResponse("berhasil")

@csrf_exempt
def delete_friend(request, npm):
    Friend.objects.filter(npm=npm).delete()
    return HttpResponse("berhasil")

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

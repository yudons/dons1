from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import *
# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_lab7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7daftarteman_url_is_exits(self):
        new_response = Client().get('/lab-7/daftar-teman/')
        self.assertEqual(new_response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_lab7daftarteman_using_index_func(self):
        found = resolve('/lab-7/daftar-teman/')
        self.assertEqual(found.func, friend_list)

    def test_model_can_create_friend(self):
        new_activity = Friend.objects.create(friend_name='yudho', npm='1406601302')
        counting_all_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_friend, 1)

    def test_can_post_addfriend(self):
        request = HttpRequest()
        response = delete_friend(request,1406601302)
        response = self.client.post('/lab-7/add-friend/', data={'name': 'YUDHO', 'npm' : '1406601300'})
        self.assertEqual(Friend.objects.all().count(), 1)
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_func(self):
        request = HttpRequest()
        response = friend_list(request)
        jsonresponse = friend_list_json(request)

    def test_model_as_dict(self):
        new_activity = Friend.objects.create(friend_name='yudho', npm='1406601302')
        dict = [obj.as_dict() for obj in Friend.objects.all()]
        self.assertEqual(dict,[{'friend_name':'yudho', 'npm':'1406601302'}])

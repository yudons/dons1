from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Yudho Prakoso'  # TODO Implement this
#curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1996,10,11)  # TODO Implement this, format (Year, Month, Date)
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date)}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_date):
    return date.today().year - birth_date.year - ((date.today().month , date.today().day) < (birth_date.month , birth_date.day))   # TODO implement this

// Calculator
var print = document.getElementById('print');
var erase = false;
var chat = document.getElementById("text");
var i;
var existingchat = JSON.parse(localStorage.getItem("allchat"));

/* ketik chat langsung keluar dan save ke localStorage */
chat.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
      var existingchat = JSON.parse(localStorage.getItem("allchat"));
      if(existingchat == null) existingchat = [];
      localStorage.setItem("chat", chat.value);
      existingchat.push(chat.value);
      localStorage.setItem("allchat", JSON.stringify(existingchat));
      var para = document.createElement("p");
      para.classList.add("msg-send");
      var node = document.createTextNode(chat.value);
      para.appendChild(node);
      var element = document.getElementById("chat");
      element.appendChild(para);
      chat.value = '';
    }
});

/* load chat yang ada di localStorage biar tampil di chat*/
for (i = 0; i < existingchat.length; i++) {
  var para = document.createElement("p");
  para.classList.add("msg-send");
  var node = document.createTextNode(existingchat[i]);
  para.appendChild(node);
  var element = document.getElementById("chat");
  element.appendChild(para);
}

var go = function(x) {
  if (x === 'ac') {
    print.value = '';
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END
